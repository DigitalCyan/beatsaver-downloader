﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace beatsaver_downloader
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void gitButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/DigitalCyan");
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
