﻿using System.IO;

namespace beatsaver_downloader
{
    static public class Config
    {
        public static Main main;
        public static string GamePath = "";
        public static string RelativeTracksPath = "Beat Saber_Data\\CustomLevels";

        public static string getFullTracksPath()
        {
            return Path.Combine(Config.GamePath, Config.RelativeTracksPath);
        }
    }
}
