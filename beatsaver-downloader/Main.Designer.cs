﻿namespace beatsaver_downloader
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.howToButton = new System.Windows.Forms.Button();
            this.aboutButton = new System.Windows.Forms.Button();
            this.changeGameDirButton = new System.Windows.Forms.Button();
            this.tracksDirButton = new System.Windows.Forms.Button();
            this.consoleTextBox = new System.Windows.Forms.RichTextBox();
            this.urlButton = new System.Windows.Forms.Button();
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.webButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.installCheckbox = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // howToButton
            // 
            this.howToButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.howToButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.howToButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.howToButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.howToButton.Location = new System.Drawing.Point(3, 124);
            this.howToButton.Name = "howToButton";
            this.howToButton.Size = new System.Drawing.Size(138, 30);
            this.howToButton.TabIndex = 7;
            this.howToButton.Text = "How to use";
            this.howToButton.UseVisualStyleBackColor = false;
            this.howToButton.Click += new System.EventHandler(this.howToButton_Click);
            // 
            // aboutButton
            // 
            this.aboutButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.aboutButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.aboutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aboutButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.aboutButton.Location = new System.Drawing.Point(3, 160);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(138, 30);
            this.aboutButton.TabIndex = 6;
            this.aboutButton.Text = "About";
            this.aboutButton.UseVisualStyleBackColor = false;
            this.aboutButton.Click += new System.EventHandler(this.aboutButton_Click);
            // 
            // changeGameDirButton
            // 
            this.changeGameDirButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.changeGameDirButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.changeGameDirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeGameDirButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.changeGameDirButton.Location = new System.Drawing.Point(3, 5);
            this.changeGameDirButton.Name = "changeGameDirButton";
            this.changeGameDirButton.Size = new System.Drawing.Size(138, 30);
            this.changeGameDirButton.TabIndex = 5;
            this.changeGameDirButton.Text = "Change Game Directory";
            this.changeGameDirButton.UseVisualStyleBackColor = false;
            this.changeGameDirButton.Click += new System.EventHandler(this.changeGameDirButton_Click);
            // 
            // tracksDirButton
            // 
            this.tracksDirButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.tracksDirButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.tracksDirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tracksDirButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.tracksDirButton.Location = new System.Drawing.Point(3, 41);
            this.tracksDirButton.Name = "tracksDirButton";
            this.tracksDirButton.Size = new System.Drawing.Size(138, 30);
            this.tracksDirButton.TabIndex = 4;
            this.tracksDirButton.Text = "Tracks Directory";
            this.tracksDirButton.UseVisualStyleBackColor = false;
            this.tracksDirButton.Click += new System.EventHandler(this.tracksDirButton_Click);
            // 
            // consoleTextBox
            // 
            this.consoleTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.consoleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.consoleTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.consoleTextBox.Location = new System.Drawing.Point(12, 213);
            this.consoleTextBox.Name = "consoleTextBox";
            this.consoleTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.consoleTextBox.Size = new System.Drawing.Size(586, 88);
            this.consoleTextBox.TabIndex = 6;
            this.consoleTextBox.Text = "";
            this.consoleTextBox.WordWrap = false;
            // 
            // urlButton
            // 
            this.urlButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.urlButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.urlButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.urlButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.urlButton.Location = new System.Drawing.Point(346, 38);
            this.urlButton.Name = "urlButton";
            this.urlButton.Size = new System.Drawing.Size(70, 31);
            this.urlButton.TabIndex = 2;
            this.urlButton.Text = "Get";
            this.urlButton.UseVisualStyleBackColor = false;
            this.urlButton.Click += new System.EventHandler(this.urlButton_Click);
            // 
            // urlTextBox
            // 
            this.urlTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.urlTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.urlTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.urlTextBox.Location = new System.Drawing.Point(48, 45);
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.Size = new System.Drawing.Size(292, 20);
            this.urlTextBox.TabIndex = 0;
            // 
            // webButton
            // 
            this.webButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.webButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.webButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.webButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.webButton.Location = new System.Drawing.Point(3, 5);
            this.webButton.Name = "webButton";
            this.webButton.Size = new System.Drawing.Size(413, 27);
            this.webButton.TabIndex = 3;
            this.webButton.Text = "Search BeatSaver";
            this.webButton.UseVisualStyleBackColor = false;
            this.webButton.Click += new System.EventHandler(this.webButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.label1.Location = new System.Drawing.Point(13, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URL";
            // 
            // installCheckbox
            // 
            this.installCheckbox.AutoSize = true;
            this.installCheckbox.Checked = true;
            this.installCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.installCheckbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.installCheckbox.Location = new System.Drawing.Point(48, 71);
            this.installCheckbox.Name = "installCheckbox";
            this.installCheckbox.Size = new System.Drawing.Size(53, 17);
            this.installCheckbox.TabIndex = 3;
            this.installCheckbox.Text = "Install";
            this.installCheckbox.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.webButton);
            this.panel1.Controls.Add(this.installCheckbox);
            this.panel1.Controls.Add(this.urlButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.urlTextBox);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(434, 195);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.aboutButton);
            this.panel2.Controls.Add(this.howToButton);
            this.panel2.Controls.Add(this.changeGameDirButton);
            this.panel2.Controls.Add(this.tracksDirButton);
            this.panel2.Location = new System.Drawing.Point(452, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(146, 195);
            this.panel2.TabIndex = 7;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(610, 318);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.consoleTextBox);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "BeatSaver Downloader";
            this.Load += new System.EventHandler(this.main_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox consoleTextBox;
        private System.Windows.Forms.Button tracksDirButton;
        private System.Windows.Forms.Button changeGameDirButton;
        private System.Windows.Forms.Button aboutButton;
        private System.Windows.Forms.Button howToButton;
        private System.Windows.Forms.Button urlButton;
        private System.Windows.Forms.TextBox urlTextBox;
        private System.Windows.Forms.Button webButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox installCheckbox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}

