﻿using System.IO;

namespace beatsaver_downloader
{
    public class Validator
    {
        public bool IsBeatSaberDir(string path)
        {
            return File.Exists(Path.Combine(path, "./Beat Saber.exe"));   
        }
    }
}
