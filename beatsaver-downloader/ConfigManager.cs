﻿using Newtonsoft.Json;
using System.IO;

namespace beatsaver_downloader
{
    public class ConfigManager
    {
        public void UpdateConfig(ConfigTemplate config)
        {
            Config.GamePath = config.GamePath;
        }

        public void WriteConfig()
        {
            ConfigTemplate config = new ConfigTemplate();
            config.GamePath = Config.GamePath;
            string serializedData = JsonConvert.SerializeObject(config);
            StreamWriter streamWriter = File.CreateText("config.json");
            streamWriter.Write(serializedData);
            streamWriter.Close();
        }

        public void LoadConfig()
        {
            StreamReader streamReader = new StreamReader("config.json");
            string serializedData = streamReader.ReadToEnd();
            streamReader.Close();
            ConfigTemplate config = JsonConvert.DeserializeObject<ConfigTemplate>(serializedData);
            UpdateConfig(config);
        }
    }
}
