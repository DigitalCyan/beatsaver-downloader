﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace beatsaver_downloader
{
    public partial class Main : Form
    {
        private Downloader downloader;
        private ConfigManager configManager;
        private Validator validator;

        // Constructor
        public Main()
        {
            InitializeComponent();
        }

        // Fires when the form loads
        private void main_Load(object sender, EventArgs e)
        {
            configManager = new ConfigManager();
            downloader = new Downloader();
            validator = new Validator();

            Configure();

            if (!Directory.Exists("./tmp"))
            {
                Directory.CreateDirectory("./tmp");
            }
        }

        private void urlButton_Click(object sender, EventArgs e)
        {
            string key = urlTextBox.Text.Split('/').Last();
            downloader.Download(key, installCheckbox.Checked);
        }

        private void changeGameDirButton_Click(object sender, EventArgs e)
        {
            SelectGamePath();
            configManager.WriteConfig();
        }

        private void Configure()
        {
            Config.main = this;

            if (!File.Exists("config.json"))
            {
                MessageBox.Show("Please select your Beat Saber installation direcotry.", "First time setup.");
                while (!SelectGamePath())
                {
                    MessageBox.Show("Please show me where your Beat Saber is installed.", "Please...");
                    continue;
                }
                ConfigTemplate config = new ConfigTemplate();
                config.GamePath = Config.GamePath;
                configManager.UpdateConfig(config);
                configManager.WriteConfig();
            }
            else
            {
                configManager.LoadConfig();
            }
            
        }

        private bool SelectGamePath()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (validator.IsBeatSaberDir(dialog.SelectedPath)) { 
                    Config.GamePath = dialog.SelectedPath;
                    return true;
                }
            }

            return false;
        }

        public void Log(string text)
        {
            consoleTextBox.Text += String.Format("{0}\n", text);
            consoleTextBox.SelectionStart = consoleTextBox.Text.Length;
            consoleTextBox.ScrollToCaret();
        }

        private void webButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://beatsaver.com/search");
        }

        private void tracksDirButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Config.getFullTracksPath());
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            new About().Show();
        }

        private void howToButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1. Find a track on beatsaver.com (HINT! There's a button that opens the page's search section instantly)\n2. Copy the page URL\n3. Paste it in the URL text box\n4. Press Get\n5. Watch the magic", "BeatSaver downloader for dummies 101");
        }
    }
}
