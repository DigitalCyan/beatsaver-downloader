﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Drawing;
using Newtonsoft.Json;

namespace beatsaver_downloader
{
    class Downloader
    {
        public void Download(string key, bool install = true, bool postInstallClean = true)
        {
            string tracksPath = Config.getFullTracksPath();
            string url = String.Format("https://beatsaver.com/api/download/key/{0}", key);

            byte[] byteData = {};

            Config.main.Log(String.Format("Searching for {0}...", key));
            using (var client = new WebClient())
            {
                client.Headers.Add("User-Agent: Other");
                try {
                    byteData = client.DownloadData(String.Format("https://beatsaver.com/api/maps/detail/{0}", key));
                }
                catch
                {
                    Config.main.Log(String.Format("Could not find {0}", key));
                    return;
                }
            }

            string stringData = Encoding.Default.GetString(byteData);
            var data = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(stringData);
            string songName = (string)(data["metadata"]["songName"]);

            Config.main.Log(String.Format("Found {0}", songName));

            string fileName = url.Split('/').Last();

            Config.main.Log(String.Format("Downloading {0}...", songName));
            using (var client = new WebClient())
            {
                client.Headers.Add("User-Agent: Other");
                client.DownloadFile(url, String.Format("./tmp/{0}.zip", fileName));
            }

            
            if (install)
            {
                Config.main.Log(String.Format("Installing {0}...", songName));
                string trackPath = Path.Combine(tracksPath, songName);
                Directory.CreateDirectory(trackPath);
                try
                {
                    ZipFile.ExtractToDirectory(String.Format("./tmp/{0}.zip", fileName), trackPath);
                }
                catch
                {
                    Config.main.Log(String.Format("{0} is already installed.", songName));
                    return;
                }

                if (postInstallClean)
                {
                    Config.main.Log("Cleaning downloaded file");
                    File.Delete(String.Format("./tmp/{0}.zip", fileName));
                }
            }

            Config.main.Log(install ? "Installation successful" : "Download sucessful");
        }
    }
}
